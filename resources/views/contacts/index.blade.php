@extends ('layouts.app')
@section ('content')
    <div class="container-fluid mr-3 mt-3">
        <div class="col-md-12 table-responsive-sm contact-list">
            @include('partials.contact-list')
        </div>
        @include('modals.deleteModal')
    </div>
@endsection

