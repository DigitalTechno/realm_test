@extends ('layouts.app')
@section ('content')
    <div class="container-fluid m-2">
        <h3>Add new Contact</h3>
        <form action="{{route('store.contact')}}" method="post" >
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group col-md-4">
                <label for="exampleInputEmail1">FirstName</label>
                <input type="text" name="first_name" class="form-control" placeholder="Enter first name">
            </div>
            <div class="form-group col-md-4">
                <label for="exampleInputPassword1">LastName</label>
                <input type="text" name="last_name" class="form-control" placeholder="Enter last name">
            </div>
            <h4 class="col-md-12">Contact details</h4>
            <hr>
            <div class="details-list">
                @include('partials.contact_details')
            </div>
            <a href="#" class="add-detail btn btn-sm btn-outline-dark ml-lg-5"><i class="fas fa-plus-circle fa-2x"></i></a></br>
            <button type="submit" class="btn btn-sm btn-dark mt-3 text-right">Create Contact</button>
        </form>
    </div>
@endsection

