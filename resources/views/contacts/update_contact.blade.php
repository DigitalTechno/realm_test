@extends ('layouts.app')
@section ('content')
    <div class="container-fluid">
        <form action="{{route('update.contact', $contact->id)}}" method="post" >
            @csrf
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group col-md-4">
                <label for="exampleInputEmail1">FirstName</label>
                <input type="text" name="first_name" required value="{{$contact->first_name}}" class="form-control" placeholder="Enter first name">
            </div>
            <div class="form-group col-md-4">
                <label for="exampleInputPassword1">LastName</label>
                <input type="text" name="last_name" required value="{{$contact->last_name}}" class="form-control" placeholder="Enter last name">
            </div>
            <h4 class="col-md-12">Contact details</h4>

            <div class="contact-details">
                <hr>
                @foreach($contact->details as $detail)
                    @include('partials.contact_details')
                @endforeach
            </div>
            <button type="submit" class="btn btn-sm btn-dark">Update</button>
        </form>
    </div>
@endsection

