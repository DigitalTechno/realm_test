<div class="row  m-2">
    <input name="detail_ids[]" type="hidden" value="{{isset($detail) ? $detail->id : ''}}">
    <div class="form-group col-md-4">
        <label for="exampleInputEmail1">Email</label>
        <input type="text" class="form-control" name="email[]" value="{{isset($detail) ? $detail->email : ''}}" aria-describedby="emailHelp" placeholder="Enter email">
    </div>
    <div class="form-group col-md-4">
        <label for="exampleInputPassword1">Contact Number</label>
        <input type="text" class="form-control" name="contact_number[]" value="{{isset($detail) ? $detail->contact_number : ''}}" placeholder="Enter Contact Number">
    </div>
    <hr>
</div>
