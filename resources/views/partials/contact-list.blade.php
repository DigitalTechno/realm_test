<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($contacts as $key => $contact)
        <div class="row">
            <tr>
                <th scope="row">{{$key+1}}</th>
                <td>{{$contact->first_name}}</td>
                <td>{{$contact->last_name}}</td>
                <td>
                    <a class="btn btn-sm btn-dark" href="{{route('edit.contact', $contact->id)}}">Update</a>
                    <button type="button" class="ml-2 btn btn-sm btn-danger delete-contact" data-id="{{$contact->id}}" data-toggle="modal" data-target="#deleteModal" >Delete</button>
                </td>
            </tr>
        </div>
    @endforeach
    </tbody>
</table>

<div class="row">
    {{ $contacts->links() }}
</div>
