<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{route('home')}}">Realm</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}">Contacts <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="{{route('create.contact')}}">Create Contact <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        @if(Request::is('/'))
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" autocomplete="off" id="search-contact" type="search" name="searchTerm" placeholder="Search" aria-label="Search">
            </form>
        @endif
    </div>
</nav>
