<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ContactController@index')->name('home');
Route::get('/create/contact', 'ContactController@createContact')->name('create.contact');
Route::post('/update/contact/{id}', 'ContactController@updateContact')->name('update.contact');
Route::get('/delete/contact/{id}', 'ContactController@deleteContact')->name('delete.contact');
Route::get('/edit/contact/{id}', 'ContactController@editContact')->name('edit.contact');
Route::post('/store', 'ContactController@storeContact')->name('store.contact');
Route::get('/search', 'ContactController@searchContacts')->name('search');
Route::get('/add/contact/detail', 'ContactController@addContactDetailRow');
