<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactDetail;
use App\Contact;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function index() {

        $contacts = Contact::paginate(10);
        return view('contacts.index', compact('contacts'));
    }

    public function createContact() {
        return view('contacts.create_contact', compact('contact'));
    }

    public function editContact($id) {
        $contact = Contact::find($id);
        $contact->details = ContactDetail::where('contact_id', $id)->get();
        return view('contacts.update_contact', compact('contact'));
    }

    public function updateContact(Request $request, $id) {
        $request->validate([
            'email.*' => 'email|max:255'
        ]);
        $contact = Contact::find($id);
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->save();

        $this->storeContactDetail($contact->id, $request);
        $contact->details = ContactDetail::where('contact_id', $id)->get();
        toastr()->success('Contact has been updated!');

        return redirect('/');
    }

    public function storeContact(Request $request) {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email.*' => 'email|max:255'
        ]);
        $contact = new Contact();
        $contact->first_name =$request->first_name;
        $contact->last_name =$request->last_name;
        $contact->save();

        $this->storeContactDetail($contact->id, $request);

        toastr()->success('Contact has been saved!');
        return redirect('/');
    }

    public function storeContactDetail($contact_id, $request) {
        foreach ($request->detail_ids as $key => $id) {
            if(isset($id)) {
                $contact_detail = ContactDetail::find($id);
            } else {
                $contact_detail = new ContactDetail();
            }
            $contact_detail->contact_id = $contact_id;
            $contact_detail->email = $request->email[$key];
            $contact_detail->contact_number = $request->contact_number[$key];
            $contact_detail->save();
        }
    }

    public function searchContacts(Request $request, Contact $contact) {
        $query = $contact->newQuery();
        if($request->has('searchTerm') && $request->searchTerm !== null) {
            $query->where('first_name', 'Like',  '%' . $request->searchTerm . '%')
                ->orWhere('last_name', 'Like',  '%' . $request->searchTerm . '%');
        }
        $contacts = $query->paginate(10);
        if($contacts) {
            foreach ($contacts as $contact) {
                $contact->details = contactDetail::where('contact_id', $contact->id)->get();
            }
            $payload = ['contacts' => $contacts];

            $html = \View::make('partials.contact-list')->with($payload)->render();
        }  else{
            toastr()->error('No Matches!');
        }

        return \Response::json(array(
            'html' => $html
        ));
    }

    public function deleteContact($id) {
        $contact = Contact::find($id);
        $contact->delete();
        ContactDetail::where('contact_id', $id)->delete();
        $contacts = Contact::paginate(10);
        $payload = ['contacts' => $contacts];

        $html = \View::make('partials.contact-list')->with($payload)->render();
        return \Response::json(array(
            'html' => $html
        ));
    }

    public function addContactDetailRow() {
        $html = \View::make('partials.contact_details')->render();
        return \Response::json(array(
            'html' => $html
        ));
    }

}
