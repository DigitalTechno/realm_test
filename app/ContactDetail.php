<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetail extends Model
{
    protected $fillable = [
        'contact_number', 'email',
    ];

    protected $table='contact_details';
}
