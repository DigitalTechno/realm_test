$(document).ready(function() {
    $(document).on('input', '#search-contact', function () {
        searchTerm =$(this).val();
        $.ajax({
            url: "/search",
            type: 'GET',
            data: {searchTerm: searchTerm},
            success: function(response) {
                $('.contact-list').html(response.html);
            },
            error: function(data) {
            }
        })
    });

    $(document).on('click', '.delete-contact', function () {
        contact_id =$(this).attr('data-id');
        $('input[name="contact_id"]').val(contact_id);
    });

    $(document).on('click', '.confirm-delete', function () {
        contact_id =$('input[name="contact_id"]').val();
        $('#deleteModal').modal('hide');
        $.ajax({
            url: "/delete/contact/"+contact_id,
            type: 'GET',
            data: {},
            success: function(response) {
                $('.contact-list').html(response.html);
                toastr.success('Contact has been deleted!');
            },
            error: function(data) {}
        })
    });

    $(document).on('click', '.add-detail', function () {
        $.ajax({
            url: "/add/contact/detail",
            type: 'GET',
            data: {},
            success: function(response) {
                $('.details-list').append(response.html);
            },
            error: function(data) {}
        })
    });
});
